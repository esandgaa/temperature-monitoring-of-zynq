# Temperature monitoring of the Zynq UltraScale+ 

This code shows the temperature for the Xilinx ZYNQ UltraScale+ ZU7EV-1FBVB900E in a simple GUI. The data is found in the SYSMONE4 architecture present on the Zynq, which provides three temperature sensing channels; One in the low power domain (LPD), one in the full power domain (FPD), and one in the programmable logic (PL). These three temperatures 

This program makes use of a peer-to-peer TCP connection. Here, the remote PC will operate as the server and the EMP as the client. The client will fetch data from the EMP's SYSMONE4 architecture and convert the data to a readable temperature using equation 2-12 on page 41 in ref 1. The equation is also shown here: 

((data_fetched * 509.3140064)/65536.0)-280.23

When the data is converted, it is sent to the server. Here, the remote PC runs Python with a relatively simple GUI to display the temperature.

# Usage
1. Run temperatureMonitor_server on the remote PC.
2. Run temperatureMonitor_client on the EMP.
If everything goes well, the user will get a confirmation in the terminal, and a small window with the temperature will appear.

# Used references
1: https://docs.xilinx.com/v/u/en-US/ug580-ultrascale-sysmon
2: https://docs.xilinx.com/api/khub/documents/HjTujqR1~G24DEplgWaT4g/content?Ft-Calling-App=ft%2Fturnkey-portal&Ft-Calling-App-Version=3.11.23&filename=ug1085-zynq-ultrascale-trm.pdf#page=189&zoom=100,72,160
3: https://docs.xilinx.com/v/u/en-US/ds891-zynq-ultrascale-plus-overview
